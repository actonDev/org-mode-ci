#!/bin/bash

set -x
docker build . -t org-mode-docker
docker run --rm -i -t -v $(pwd):$(pwd) --workdir=$(pwd) org-mode-docker emacs --batch -load emacs.el --file "$1" -f org-latex-export-to-pdf >& log

cat log
