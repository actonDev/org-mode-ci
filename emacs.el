(setq org-latex-listings 'minted
	  org-latex-packages-alist '(("" "minted"))
	  )

(setq org-latex-pdf-process 
	  '("xelatex -shell-escape -interaction=nonstopmode %f"
		))
